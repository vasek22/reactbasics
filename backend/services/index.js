  
'use strict';

const auth = require('./auth.js');
const users = require('./users.js');
const ideas = require('./ideas.js');

module.exports = app => {
  app.configure(users);
  app.configure(ideas);
  app.configure(auth);
};