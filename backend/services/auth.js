const { AuthenticationService } = require('@feathersjs/authentication');
const { LocalStrategy }  = require('@feathersjs/authentication-local')

class MyLocalStrategy extends LocalStrategy {
  /*async getEntityQuery(query, params) {
    return {
      ...query,
      role: 'admin',
      $limit: 1
    }
  }*/

  comparePassword(entity, password) {
    // console.log(entity, password);
    return new Promise((resolve, reject) => {
        if ((entity.password !== password)) {
          return reject(false);
        }

        return resolve(entity);
      });
  }
}

module.exports = app => {
  const authService = new AuthenticationService(app);

  authService.register('local', new MyLocalStrategy());

  app.use('/authentication', authService);
}