  
const { DataTypes } = require('sequelize');
const createService  = require('feathers-sequelize');

module.exports = app => {
  const sequelize = app.get('mysql');
  const IdeaService = sequelize.define('ideas', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
      allowNull: false
    },
    text: {
      type: DataTypes.STRING,
      allowNull: false
    },
    tech: {
      type: DataTypes.STRING,
      allowNull: false
    },
    viewer: {
      type: DataTypes.STRING,
      allowNull: false
    }
  }, {
    freezeTableName: true
  });

  // Initialize our service with any options it requires
  app.use('/ideas', createService({
    Model: IdeaService,
    /*paginate: {
      default: 2,
      max: 4
    }*/
  }));

  // creating first idea
  /*IdeaService.sync({ force: true }).then(() => {
  // Create a dummy Message
  app.service('ideas').create({
    text: 'Build a cool app',
    tech: 'Node.js',
    viewer: 'John Doe'
  }).then(message => console.log('Created message', message))
  .catch(err => console.log(err));
  })
  .catch(err => console.log(err));*/

};