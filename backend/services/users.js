// Initializes the `users` service on path `/users`
const { DataTypes } = require('sequelize');
const createService  = require('feathers-sequelize');

module.exports = app => {
  const sequelize = app.get('mysql');
  const UsersService = sequelize.define('users', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
      allowNull: false
    },
    username: {
      type: DataTypes.STRING,
      allowNull: false
    },
    password: {
      type: DataTypes.STRING,
      allowNull: false
    },
    role: {
      type: DataTypes.ENUM(['admin', 'manager', 'common']),
      allowNull: false
    }
  }, {
    freezeTableName: true
  });

  // Initialize our service with any options it requires
  app.use('/users', createService({
    Model: UsersService,
    /*paginate: {
      default: 2,
      max: 4
    }*/
  }));

  // creating first idea
  /*UsersService.sync({ force: true }).then(() => {
  // Create a dummy Message
  app.service('users').create({
    username: 'vasek',
    password: '1234',
    role: 'admin'
  }).then(message => console.log('Created message', message))
  .catch(err => console.log(err));
  }).catch(err => console.log(err));*/
}
