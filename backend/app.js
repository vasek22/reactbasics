const feathers = require('@feathersjs/feathers');
const express = require('@feathersjs/express');
const socketio = require('@feathersjs/socketio');
const { Sequelize } = require('sequelize');
const config = require('./config/auth.json');

const services = require('./services');

const sequelize = new Sequelize('react_learn', 'root', '', {
  host: 'localhost',
  dialect: 'mysql',
  //logging: (...msg) => console.log(msg),
  logging: console.log,
});

const app = express(feathers());

// Parse JSON
app.use(express.json());
// Config Socket.io realtime APIs
app.configure(socketio());
// Enable REST services
app.configure(express.rest());
// Add DB to app
app.set('mysql', sequelize);
// auth config
app.set('authentication', config.authentication)
// Register services
app.configure(services);

// New connections connect to stream channel
app.on('connection', conn => app.channel('stream').join(conn));
// Publish events to stream
app.publish(data => app.channel('stream'));

const PORT = process.env.PORT || 3030;

app.listen(PORT)
  .on('listening', () =>
    console.log(`Realtime server running on port ${PORT}`)
  );

// creating first idea
/* IdeaService.sync({ force: true }).then(() => {
  // Create a dummy Message
  app.service('ideas').create({
    text: 'Build a cool app',
    tech: 'Node.js',
    viewer: 'John Doe'
  }).then(message => console.log('Created message', message))
  .catch(err => console.log(err));
})
.catch(err => console.log(err)); */
