import React, { Component } from "react";
import { Switch, Route } from "react-router-dom";
import WelcomePage from './components/WelcomePage';
import Main from './components/Main';

class App extends Component {
  render() {
    console.log(this.props)
    const { location } = this.props;

    return (
      <Switch location={location}>
        <Route path="/login" component={Main} />
        <Route path="/private" component={Main} />
        <Route path="/" component={WelcomePage} />
      </Switch>
    );
  }
}

export default App;