import feathers from '@feathersjs/feathers';
import socketio from '@feathersjs/socketio-client';
import io from 'socket.io-client';
import auth from '@feathersjs/authentication-client';

const host = 'http://localhost:3030';
const socket = io(host);

const app = feathers();

app.configure(socketio(socket));

app.configure(auth());

export default app;
