import React from 'react';
import ReactDOM from 'react-dom';
import { Route, BrowserRouter } from "react-router-dom";
import { ApplicationStateProvider } from "./ApplicationStateProvider";
import App from './App';

ReactDOM.render(
  <BrowserRouter>
    <ApplicationStateProvider>
      <Route component={App} />
    </ApplicationStateProvider>
  </BrowserRouter>,
  document.getElementById('root')
);