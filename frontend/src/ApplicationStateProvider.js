import React, { Component } from "react";
//import DataService from "./DataService";
import app from './feathers';

const ApplicationContext = React.createContext();

export class ApplicationStateProvider extends Component {
  constructor() {
    super();

    let user = null;
    try {
      user = JSON.parse(localStorage.getItem("user"));
    } catch (error) {}

    if (user) {
      const now = Date.now();
      console.log(user);
      if (user.time && user.time - now < 0) {
        user = null;
      }
    }

    this.state = {
      authenticationError: null,
      login: this.login,
      user
    };
  }

  login = async ({ username, password, redirect }) => {
    try {

      const response = await app.authenticate({
        strategy: 'local',
        username,
        password
      });
      
      console.log(response);

      const data = {
        data: response.undefined, 
        expiration: 1000000
      }
      return this.onAuthSuccess(data, redirect);
    } catch (error) {
      return this.onAuthFailure(error);
    }
  };

  onAuthSuccess = (data, redirect) => {
    const user = data.data;
    user.time = Date.now() + user.expiration;

    this.setState({
      user,
      authenticationRequired: false,
      authenticationError: null
    });

    localStorage.setItem("user", JSON.stringify(user));

    if (redirect) {
      redirect();
      return;
    }

    /*data.notifications.forEach(notification => {
      console.log(notification);
    });*/
  };

  onAuthFailure = (error) => {
    if (!error.response) {
      error.response = {};
    }

    this.setState({
      authenticationError: {
        status: error.response.status,
        statusText: error.response.statusText,
        message: error.response.data
      },
      authenticated: false,
      authenticationRequired: true
    });

    if (error.response && error.response.status === 401) {
      console.error('Error 401');
      return;
    }

    console.error('Error', error);
  };

  logout = () => {
    sessionStorage.removeItem("user");
    localStorage.removeItem("user");
    this.setState({
      authenticated: false,
      authenticationRequired: true,
      user: null
    });
  };

  render() {
    const { children } = this.props;
    const { user, authenticationRequired, authenticationError } = this.state;

    return (
      <ApplicationContext.Provider
        value={{
          login: this.login,
          onAuthSuccess: this.onAuthSuccess,
          onAuthFailure: this.onAuthFailure,
          user,
          authenticated: user !== null,
          authenticationRequired,
          authenticationError,
          logout: this.logout
        }}
      >
        {children}
      </ApplicationContext.Provider>
    );
  }
}

export const ApplicationStateConsumer = ApplicationContext.Consumer;