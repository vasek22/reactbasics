import React from "react";
import Header from './Header';
import LoginForm from './LoginForm';

const Login = ({ login, onAuthSuccess, onAuthFailure, redirect }) => {
  return (
    <div>
      <Header />
      <div className="container mt-5">
        <div className="row">
          <LoginForm
            login={login}
            redirect={redirect}
            onAuthSuccess={onAuthSuccess}
            onAuthFailure={onAuthFailure}
          />
        </div>
      </div>
    </div>
  )
}

export default Login;