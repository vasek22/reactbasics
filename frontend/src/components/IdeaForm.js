import React, { Component } from 'react';

import app from '../feathers';

class IdeaForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            text: '',
            tech: '',
            viewer: '',
        }
    }

    handleInput = (event, type) => {
        const newState = {};
        newState[type] = event.target.value;
        this.setState(newState);
    }

    handleSubmit = (event) => {
        event.preventDefault();
        console.log('create', this.state);

        // Create new idea
        app.service('ideas').create(this.state);
  
        // Clear inputs
        this.setState({
            text: '',
            tech: '',
            viewer: '',
        });
    }

    render() {
        return (
            <form id="form" onSubmit={this.handleSubmit}>
                <div className="form-group">
                <input
                    onChange={(event) => this.handleInput(event, 'text')}
                    value={this.state.text}
                    type="text"
                    id="idea-text"
                    className="form-control bg-dark text-white"
                    placeholder="Enter idea (30 chars max)"
                    maxLength="30"
                    required
                />
                </div>
                <div className="form-group">
                <input
                    onChange={(event) => this.handleInput(event, 'tech')}
                    value={this.state.tech}
                    type="text"
                    id="idea-tech"
                    className="form-control bg-dark text-white"
                    placeholder="Language, framework, etc"
                    maxLength="30"
                    required
                />
                </div>

                <div className="form-group">
                <input
                    onChange={(event) => this.handleInput(event, 'viewer')}
                    value={this.state.viewer}
                    type="text"
                    id="idea-viewer"
                    className="form-control bg-dark text-white"
                    placeholder="Enter your name"
                    maxLength="20"
                    required
                />
                </div>

                <button type="submit" className="btn btn-primary btn-block">
                    Suggest Idea
                </button>
            </form>
        )
    }
}

export default IdeaForm;