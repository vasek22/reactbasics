import React from 'react';
import { Link } from "react-router-dom";
import { ApplicationStateConsumer } from "../ApplicationStateProvider";

function Header() {
  return (
    <ApplicationStateConsumer>
      {({ authenticated, logout }) => (
        <nav className="navbar navbar-light bg-light">
          <div className="container">
            <Link to="/" >
              <span className="navbar-brand mb-0 h1">Livestream Project Ideas</span>
            </Link>
            <div>
              {!authenticated && (
                <Link to="/login">
                  <button className="btn btn-link">Přihlásit se</button>
                </Link>
              )}
              {authenticated && (
                <React.Fragment>
                  <button className="btn btn-link" onClick={logout}>Odhlásit se</button>
                  <Link to="/private">
                    <button className="btn btn-link">Idea list</button>
                  </Link>
                </React.Fragment>
              )}
            </div>
          </div>
        </nav>
      )}
    </ApplicationStateConsumer>
  )
}

export default Header;