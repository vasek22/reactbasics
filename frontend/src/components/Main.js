import React, { Component } from 'react';
import withAuthentication from "../withAuthentication";
import Header from './Header';
import IdeaForm from './IdeaForm';
import IdeaShow from './IdeaShow';

import app from '../feathers';

class Main extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ideas: []
    }
    this.init();
  }

  async init() {
    const ideas = await app.service('ideas').find();
    this.setState({ ideas });
  }

  componentDidMount() {
    const ideaService = app.service('ideas');
    ideaService.on('created', (idea) => {
      this.state.ideas.push(idea);
      this.setState(this.state);
    });
  }

  render() {
    return (
      <div>
        <Header />
        <div className="container mt-5">
          <div className="row">
            <div className="col-md-6">
              <h1 className="text-center mb-3">
                Submit an Idea
              </h1>
              <IdeaForm />
            </div>
            <div className="col-md-6">
              <IdeaShow ideas={this.state.ideas}/>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default withAuthentication(Main);

