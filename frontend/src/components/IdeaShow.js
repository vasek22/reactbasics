import React from 'react';

function IdeaShow(props) {
  console.log(props);
  return <div id="ideas">
    {props.ideas.map((idea, index) => {
        return <div key={index} className="card bg-secondary my-3">
          <div className="card-body">
            <p className="lead">
              {idea.text} <strong>({idea.tech})</strong>
              <br />
              <em>Submitted by {idea.viewer}</em>
              <br />
              <small>{idea.createdAt}</small>
            </p>
          </div>
        </div>
      })}
  </div>
}
  
export default IdeaShow;