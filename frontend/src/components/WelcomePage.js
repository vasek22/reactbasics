import React, { Component } from 'react';
import Header from './Header';

class WelcomePage extends Component {
    render() {
        return (
            <div>
                <Header />
                <div className="container mt-5">
                    <div className="row">
                        <h1 className="text-center mb-3">
                            Vítej kámo!
                        </h1>
                    </div>
                </div>
            </div>
        )
    }
}

export default WelcomePage;