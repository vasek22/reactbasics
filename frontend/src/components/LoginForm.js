import React, { Component } from "react";
import '../css/style.css';

class LoginForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            username: '',
            password: '',
        }
        console.log(props);
    }

    handleInput = (event, type) => {
        const newState = {};
        newState[type] = event.target.value;
        this.setState(newState);
    }

    handleSubmit = (event) => {
        event.preventDefault();
        console.log('login', this.state, this.props);

        this.props.login(
            {
                username: this.state.username,
                password: this.state.password,
                redirect: this.props.redirect
            }
        );
    }

    render() {
        return (
            <form id="form" onSubmit={this.handleSubmit}>
                <div className="form-group">
                <input
                    onChange={(event) => this.handleInput(event, 'username')}
                    value={this.state.text}
                    type="text"
                    id="username-text"
                    className="form-control bg-dark text-white"
                    placeholder="Username"
                    maxLength="50"
                    required
                />
                </div>
                <div className="form-group">
                <input
                    onChange={(event) => this.handleInput(event, 'password')}
                    value={this.state.tech}
                    type="password"
                    id="password-tech"
                    className="form-control bg-dark text-white"
                    placeholder="Password"
                    required
                />
                </div>

                <button type="submit" className="btn btn-primary btn-block">
                    Login
                </button>
            </form>
        )
    }
}

export default LoginForm;