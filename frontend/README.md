# React + FeathersJS (Socket.io)

> Simple app for coding livestreamers to have viewers send in project ideas in real-time

## Quick Start

```bash
# Install dependencies
npm install

# Serve on localhost:3030
npm start

# Open index.html
```

### Version

1.0.0

### License

This project is licensed under the MIT License

### Libraries

* React :  https://reactjs.org
* React Router: https://reactrouter.com
* Feathersjs : https://docs.feathersjs.com
